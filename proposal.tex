\documentclass[11pt]{article}



\usepackage{xspace,relsize,bibunits}
\usepackage[square,comma]{natbib}
\usepackage{times,graphicx,paralist}


\usepackage{url,ifthen}


\usepackage{titlesec}

\titleformat*{\section}{\large\bfseries}
\titleformat*{\subsection}{\bfseries}
\titleformat*{\subsubsection}{\bfseries}

\newcommand{\system}[1]{\texttt{#1}\xspace}

\newcommand{\lex}[1]{\textit{#1}}
\newcommand{\gl}[1]{``#1''}
\newcommand{\ex}[2][***]{\ifthenelse{\equal{#1}{***}}%
{\lex{#2}}{\lex{#2} \gl{#1}}}


\setlength\topmargin{0pt}
\addtolength\topmargin{-\headheight}
\addtolength\topmargin{-\headsep}
\setlength\oddsidemargin{0pt}
\setlength\textwidth{\paperwidth}
\addtolength\textwidth{-2in}
\setlength\textheight{\paperheight}
\addtolength\textheight{-2in}
\usepackage{layout}


%\textwidth=175mm                % 165mm
%\textheight=247mm               % 225mm
%\columnsep=10mm                 % 10mm
%\oddsidemargin=-10pt          % Edit this to fit your paper
%\evensidemargin=-10pt         % Edit this to fit your paper
%\topmargin=-50mm                % -10mm
%
%\clubpenalty=450
%\widowpenalty=600


\newcommand{\eng}[1]{\textit{#1}\xspace}

\newcommand{\class}[1]{\textsc{#1}\xspace}

\newcommand{\sparagraph}[1]{\vspace*{-2.5ex}\paragraph*{#1}}

\providecommand{\keywords}[1]
{
  \small	\textbf{\textit{Keywords ---}} #1
}



\pagestyle{empty}


\begin{document}






\begin{center}
  \large \textbf{Fair, Calibrated, Interpretable Models for
    Semi-automatic Legal Assistance Triage} \\[1ex]
  \smaller
  \textbf{PI:} Jey Han Lau\\
  \textbf{Co-PI:} Timothy Baldwin\\
  School of Computing and Information Systems \\
  The University of Melbourne, VIC 3010 Australia \\
  \url{{laujh,tbaldwin}@unimelb.edu.au}\\[1ex]

  Cash funding needed: \$80,000 \\
  AWS Promotional Credits needed: \$20,000 \\

\end{center}

\begin{abstract}
In Australia alone, the number of people with unmet legal needs is 
estimated to be over four million people per year (around 20\% of the 
population) and growing, and free legal assistance services are severely 
under-resourced. A critical bottleneck for free legal assistance 
providers is the determination of what (if any) specific legal needs the 
individual has, to which end we propose to develop NLP models to 
semi-automate the process. Critically, we will develop models that are 
fair and well calibrated across different sub-populations (esp.\ for the 
historically disadvantaged), and interpretable so as to be able to 
solicit user feedback in cases where the nature of the issue is unclear.
\end{abstract}

\keywords{natural language processing, fairness, calibration, 
interpretability, bias mitigation, text classification}


\section{Research Goal and Problem Statement}

Pro bono legal assistance is severely under-resourced, and in the
Australian context, it is estimated that around 50\% of legal needs are
unmet \citep{law-survey:2012,justice-review:2012}. Unresolved legal
problems have been shown to lead to significant life impacts, including
financial strain (29\%), stress-related illness (20\%), physical ill
health (19\%), relationship breakdown (10\%), and having to move home
(5\%) \citep{legal-survey:2012}. A critical bottleneck in the legal
assistance domain is the determination of the nature of the problem
(esp.\ difficult given that most users of such services have little or
no formal experience with the legal system), whether it is legal in
nature, and what legal services are relevant. Natural language
processing can potentially help to alleviate this bottleneck, in
providing semi-automatic legal triage assistance in the form of
predictive models applied to user-supplied textual descriptions of the
issue, trained on historical data that has been extensively labelled at
the document- and span-levels by legal experts. The goal of
this project is the development of methods to learn \textbf{fair,
  accurate, well-calibrated, and human-interpretable models for
  semi-automatic legal assistance triage} in terms of labelling a request
  for legal assistance with relevant areas of law, supporting
non-expert human-in-the-loop labelling by the user in the instance that
the model is uncertain of its predictions.

% Naively-trained NLP models are often biased in various problematic ways,
% including performing substantially worse for
% under-represented/socially-disadvantaged demographics \citep{}. This is
% a particular problem in the legal domain because the quality of legal
% service provision will likely be compromised for the most
% socially-vulnerable users (the precise demographics who are in greatest
% need of legal help).

The context for this project is the suite of legal services offered by
Justice Connect, an Australian-based charity and public
benevolent institution which designs and delivers high-impact
interventions to increase access to legal support and achieve social
justice. Relevant to this project, since 2018, Justice Connect has been
running a web-based intake system to assist applicants in applying for
free legal services via its extensive network of pro bono legal service
providers. In 2020, the tool serviced around 40,000 legal requests
(more than double the number in 2019, and there has been similar growth
in 2021), which were manually assessed and
triaged by lawyers by labelling the requests based on 33 area-of-law
categories, which are then used to route the requests to over 600 member
firms who provide pro bono legal assistance (with a combined total of nearly
50,000 pro bono hours in 2020) \citep{JC-annual-report:2020}. The expert
time required to process this ever-increasing volume of requests has
generated a critical bottleneck for Justice Connect, which this project
aims to alleviate with supervised topic classification models.

The Justice Connect intake system collects basic demographic information
as it is pertinent to accessing federal funding (e.g.\ is the user a
pensioner, or identify as an Aboriginal or Torres Strait Islander?), in
addition to opt-in personal information to help Justice Connect monitor
progress towards its social justice objectives (e.g.\ does the user
identify as LGBTQI+?).  As part of a seed project with Justice Connect
where we have been training topic classification models over historical
data using pre-trained language models, we have identified systematic
drops in performance for disadvantaged groups such as pensioners and
Aboriginal or Torres Strait Islanders, and also a drop in accuracy for
areas of law heavily associated with such groups (e.g.\ \class{Elder
  law}, \class{Native title}, and \class{Migration}) 
\citep{Mistica+:2021}.  We have
additionally sourced 7-way expert document- and span-level annotations
over 4000+ help requests through Justice Connect's network of pro bono
lawyers, and used this to evaluate model calibration
\citep{Desai:Durrett:2020}, and similarly observed an inverse
correlation between degree of association with vulnerable groups and
per-class calibration (i.e.\ the model is generally \textit{least well}
calibrated for classes \textit{most heavily} associated with vulnerable
groups). This motivates our specific interest in \textbf{fairness} and
\textbf{calibration} in the legal assistance domain.

The final piece of the puzzle is \textbf{interpretability}, to be able to
automatically enlist the help of the user in cases where the nature of
the request is not clear. Specifically, we propose to identify spans of
text in the request text most indicative of a given area of law, and
highlight these to the user in clarifying whether this is indeed
indicative of a given legal need (through the use of a plain-English
template-based description of that area of law), and possibly provide
supplementary information to make this clearer. In order to achieve
this, we need expert annotations of spans of text which are most
indicative of a given area of law (with some indication of confidence
level), which is precisely what we have as part of the 4000+ annotated
help requests from Justice Connect, with the possibility of obtaining
even more in the future. In order for this to be effective for all user types, the
model needs to be fair and well calibrated at both the document and span
levels, to be able to provide truly interpretable output (which
correlates with expert annotations) and meaningfully elicit user input
(in cases of true ambiguity).




\section{Proposed approach, and Expected Outcomes and Results}
\label{method}

\subsection{Bias Mitigation for Under-represented Groups}


The standard approach to mitigating bias is to directly use private
attributes as part of training to constrain the model to be fair
\citep{li2018towards,ravfogel-etal-2020-null}, but in the case of the
Justice Connect data, we often do not have access to such data, unless
it is strictly relevant to determining eligibility or provide legal
assistance (as detailed above). We thus need to rely on \textit{proxy
  attributes} that are provided as part of the user input for
eligibility determination (e.g.\ age = 65 or over), or the output areas
of law (e.g.\ immigration law) that are highly associated with
disadvantaged groups.  These proxy attributes serve as input metadata
that the model has access to during training to incorporate into
adversarial learning, to explicitly learn representations that are
invariant to these attributes, so that post-training such attributes are
not predictable from our model. Alternatively, data annotated with
private attributes can be sourced from external domains (with less user
sensitivity), decoupling the training of adversaries from the training
of the target variable (areas of law), something we have successfully
experimented with for simpler topic classification tasks such as
sentiment analysis \citep{Han+:2021b} but which is untested for more
complex tasks. Additionally, we will experiment with methods such as
extensions of null space projection \citep{ravfogel-etal-2020-null} to
handle multiple attributes, post-training debiasing
\citep{kaneko2019gender}, bias-constrained models
\citep{cotter2019two}, and margin-based class imbalance models \citep{cikm2020}.

Beyond the limited user metadata that pre-exists in the Justice Connect
data, we will work with Justice Connect to collect demographic data for
a small sample of entries (with user consent, and for evaluation
purposes only). This will provide a means to perform more fine-grained
analysis of fairness and calibration across different disadvantaged subgroups. 


\subsection{Span-Level Prediction/Interpretability and Calibration}

The Justice Connect dataset has three sets of annotations: (1) from the help seeker themselves using a coarse-grained
``layperson'' legal classification at the document level; (2) from
expert annotators using 33 fine-grained, domain-specific areas of law,
also at the document level and with an indication of user confidence; and (3) also from expert annotators at the span level,
again based on the 33 areas of law and with an indication of user
confidence. Although the document-level areas of law (second set of
annotations) are the core information that determines how the request is
triaged, the span-level annotations help explain what gives rise to the
document-level label. Our second goal of the project is to leverage
these aannotations to build a span-based classifier, partly as
an additional supervision signal, but additionally to solicit user
feedback in cases where there is low confidence in the model prediction
or the span-level prediction contradicts the document-level prediction.

There are two challenges here: (1) the document- and
span-level labels are provided by multiple lawyers, and inevitably there
is disagreement and uncertainty in the data; and (2) deep learning
classifiers are known to be poorly calibrated, and hence do not provide
an accurate estimate of confidence or correctness
\citep{Guo+:2017,Desai:Durrett:2020}.

For the first challenge, we will first measure annotator agreement using
measures such as one-vs-rest correlation \citep{Lau+:2020} and
span-based Krippendorf's alpha \cite{Krippendorff:2011}. As these may
not provide an accurate measure of annotation quality, we will also
experiment with an annotation model
\citep{Passonneau+:2014,Mathur+:2018} that builds a probabilistic model
over the annotations, taking into account label prevalence, annotator
biases, and the self-rated confidence on the part of the annotator. This
will allow us to filter out low probability labels, keeping only high
quality ones for training our span-based classifier, and also explicitly
train using the soft labels \citep{fornaciari-etal-2021-beyond}. A
caveat to note is that these models of annotation are developed for
document classifiers --- the novelty of our research is that we will
extend them for span-based classification, which we will frame as a
sequence labelling task.  In the original data, spans of text are
highlighted by multiple lawyers that denote a particular area of law.
To model the varying nature of disagreement in the span-level annotation
(e.g.\ given the same span disagreement in the legal category, or
disagreement in the exact span but with the same legal category), we
propose to model it as a sequence labelling problem where each word in
the text is assigned to a label for each annotator, thereby facilitating
the adaptation of these document models of annotation to our problem.

For the second challenge, we will first test simple methods such as 
label smoothing to calibrate our span-based classifier, which has been 
found to be an effective approach \citep{Muller+:2019}. Drawing on 
\cite{Zhang+:2021}, we will also explore using 
conformal predictors for calibration, which works by comparing how well 
an unseen example (a span of text in our case) \textit{conforms} to 
previously seen examples (spans of text in the training data). The 
advantage of a conformal predictor is that it provides a 
theoretically-grounded estimate of risk (error rate) which label 
smoothing lacks, a feature that is critical in our application. As with 
the first challenge, the novelty of the research is here is that we will 
be developing methods to adapt conformal predictors that are designed 
for document classification to our setting (span-based classification).

Of particular interest here will be the interaction between calibration
and fairness, in terms of both jointly performing the two tasks, and a systematic evaluation of the impact
of debiasing on calibration for different user groups and vice
versa. While this interaction has been explored to some degree in
machine learning \citep{pleiss2017fairness}, it is highly novel
for NLP.


\section{Impact on Underrepresented Groups}

The Justice Connect legal assistance services is used especially heavily
by underrepresented groups, so any improvement in the efficiency and
scalability of the service (esp.\ in the face of exponential growth)
will directly benefit those groups. Social justice is a founding
principle of Justice Connect, so quantifiable levels of fairness are a
critical concern in deploying any NLP method over their data. While we
will not be able to share any of the Justice Connect data with the
research or legal service communities due to legal confidentiality, the code developed in
this project will be made available open source for other legal
assistance organisations throughout the world to deploy over their own
data, to maximise impact.


\section{Related Work}

The application of NLP in the pro bono legal assistance domain is highly
novel, but there is a plethora of related work on text classification
for legal texts of different types
\citep{chalkidis2019extreme,tuggener2020ledgar}. Fairness in the legal
domain has been explored almost exclusively in the context of legal
sentencing and recidivism
\citep{kenrick1980contrast,englich2006playing,wang2021equality}. Calibration
is a relatively new topic to NLP \citep{Desai:Durrett:2020}, and its
interactions with fairness are almost completely unexplored for
pre-trained language models. Combining document- and span-level
modelling is not new (e.g.\ joint NER and MeSH labelling), but doing so
using the same label set for interpretability purposes, and combined
with model fairness and calibration, is highly novel.


%How does your research relate to prior work in the area (including your own, if relevant)? What makes you qualified to do this research work?


\section{Funds Needed}

We request cash funding of USD\$80K to support a postdoctoral 
researcher for 12 months to perform the research as outlined in Section 
\ref{method}. The funds will be used to extend the duration of the 
postdoctoral researcher as part of an ongoing project, allowing us dive 
deeper in the exploration of bias mitigation and model calibration for 
under-represented groups, and analysis of the interaction between 
fairness and calibration in our algorithms. We also request AWS 
promotional credits of USD\$20K, which will provide ample GPU cycle for 
training deep learning classification models in our project.


{
\renewcommand{\bibsep}{0.18mm}
\bibliographystyle{plainnat}
\bibliography{project,strings,papers,collections}
}



\end{document}




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
